# Bayes_Disease_Incidence

In this R file you can have a look at an exercise solved with Bayes. We would like to estimate the incidence ratio of a disease in a population On 100 people who have been examined, we observe 6 people who have the disease. We suppose an a priori law theta ~ Be(a=1, b=10) on this unknown parameter.

Question 1 : Study the a priori law, give an interval in which there is theta a priori with a probability of 95%

Question 2 : Give the a posteriori law p(theta\x)

Question 3 : Give the a posteriori mean and the a posteriori mode 

Question 4 : Give the credibility interval at 95% on the parameter theta knowing the data

Question 5 : We add 200 new patients to the study on which we have 15 infected people. We note those data x'. Give the distribution p(theta\x,x')

Question 6 : Explain his link with p(theta\x)


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2022

